//#include "clang.hpp"
#include "ncxxforth.config.hpp"
#include "ncxxforth.hpp"

#include <cstddef>
#include <cstdlib>
#include <type_traits>
#include <variant>

namespace std {
	std::string to_string(const ncxxforth::Cell &input, const ncxxforth::Forth *f) {
		return std::visit(
		 ncxxforth::visitor{
		  [](ncxxforth::tcell::Number c) -> std::string { return std::string{"<Number>("} + std::to_string(c) + ")"; },
		  [f](ncxxforth::Word *c) -> std::string {
			  ncxxforth::tcell::StringView name = (f == nullptr ? "" : f->dictFind(c));
			  return std::string{"<Word*>("}
			       + (name.empty() ? std::to_string(reinterpret_cast<uintptr_t>(c)) : std::string{name}) + ")";
		  },
		  [](ncxxforth::tcell::NativePtr c) -> std::string {
			  return std::string{"<NativePtr>("} + std::to_string(reinterpret_cast<uintptr_t>(c)) + ")";
		  },
		  [](ncxxforth::tcell::NativeWord c) -> std::string {
			  return std::string{"<NativeWord>("} + std::to_string(reinterpret_cast<uintptr_t>(c)) + ")";
		  },
		  [](const ncxxforth::tcell::String &c) -> std::string { return std::string{"<String>("} + c + ")"; },
		  [](const ncxxforth::tcell::StringView &c) -> std::string {
			  return std::string{"<StringView>("} + std::string{c} + ")";
		  },
		 },
		 input);
	}
}

namespace ncxxforth {
	bool is_number(tcell::StringView word) {
		// negative numbers are '-' .. positive numbers
		if (word[0] == '-') return is_number(word.substr(1));

		// fastpath if first char aint a digit
		if (0 == isdigit(word[0])) return false;

		if (word.length() > 2 && 0 != word.compare(0, 1, "0x")) { // hex
			return word.substr(2).find_first_not_of("0123456789ABCDEFabcdef") == std::string::npos;
		} else { // decimal or octal
			return word.find_first_not_of("0123456789") == std::string::npos;
		}
	}

	/*constexpr*/ tcell::Number strvton(tcell::StringView str) {
		// because what even is overloading, right std::a/strto*?! fuck you too.
		if constexpr (std::is_same_v<tcell::Number, int>)
			return static_cast<int>(std::strtol(str.data(), nullptr, 0));
		else if constexpr (std::is_same_v<tcell::Number, long int>)
			return std::strtol(str.data(), nullptr, 0);
		else if constexpr (std::is_same_v<tcell::Number, long long int>)
			return std::strtoll(str.data(), nullptr, 0);
	}

	tcell::StringView Forth::dictFind(Word *ptr) const {
		auto it = std::find_if(std::begin(dict), std::end(dict), [ptr](auto &&p) { return p.second == ptr; });
		if (it == std::end(dict)) return "";
		return it->first;
	}

	Forth::Forth() {}

	Forth::Forth(Dictionary _dict, tcell::StringView _init) : dict(_dict) { eval(_init); }

	Forth::~Forth() {}

	void Forth::eval(tcell::StringView source) {
		auto temp_offs = src_offs;
		src.swap(source);
		src_offs = 0;

		auto old_comp = compiling;
		compiling     = nullptr;

		eval_inplace();

		compiling = old_comp;

		src.swap(source);
		src_offs = temp_offs;
	}

	void Forth::eval_inplace() {
		while (src_offs < src.length()) {
			auto word = read_word();
			interp1(word);
		}
	}

	Word Forth::compile(tcell::StringView source) {
		auto temp_offs = src_offs;
		src.swap(source);
		src_offs = 0;

		Word out = compile_inplace();

		src.swap(source);
		src_offs = temp_offs;

		return out;
	}

	Word Forth::compile_inplace() {
		Word out;
		auto old_comp = compiling;
		compiling     = &out;

		while (src_offs < src.length()) {
			auto word = read_word();
			comp1(word);
			if (breaks > 0) {
				breaks--;
				break;
			}
		}

		compiling = old_comp;
		return out;
	}

	tcell::String Forth::read_word(tcell::StringView del) {
		tcell::String out;

		// skip leading delims
		for (; src_offs < src.length() && del.find(src[src_offs]) != std::string::npos; src_offs++)
			;
		// copy chars
		for (; src_offs < src.length() && del.find(src[src_offs]) == std::string::npos; src_offs++) {
			out.push_back(src[src_offs]);
		}
		// skip one trailing delim
		if (src_offs < src.length() && del.find(src[src_offs]) != std::string::npos) src_offs++;

		//	std::cerr << "Read: <" << out << ">" << std::endl;

		return out;
	}

	void Forth::comp1(tcell::StringView word) {
		tcell::String sword(word);
		if (is_number(word)) {
			compiling->content.push_back(strvton(word));
		} else if (dict.contains(sword)) {
			Word *w = dict.at(sword);
			if (w->immediate)
				exec1(*dict.at(sword));
			else
				compiling->content.push_back(w);
		} else {
			//	std::cerr << "Word " << word << " not found." << consts::svENDL;
			//	return std::abort();
		}
	}

	void Forth::interp1(tcell::StringView word) {
		tcell::String sword(word);
		if (is_number(word)) {
			stack.emplace(strvton(word));
		} else if (dict.contains(sword)) {
			return exec1(*dict.at(sword));
		} else {
			//	std::cerr << "Word " << word << " not found." << consts::svENDL;
			//	return std::abort();
		}
	}

	void Forth::exec1(Word &word) {
		auto old_parent = exec_parent;
		exec_parent     = executing;
		executing       = &word;
		for (size_t i = 0; i < word.content.size(); i = static_cast<size_t>(static_cast<tcell::Number>(i) + jumps)) {
			auto step = word.content.at(i);
			breaks    = 0;
			jumps     = 1;
			std::visit(visitor{
			            [this](ncxxforth::tcell::Number c) { stack.push(c); },
			            [this](ncxxforth::Word *c) { exec1(*c); },
			            [this](ncxxforth::tcell::NativePtr c) { stack.push(c); },
			            [this](ncxxforth::tcell::NativeWord c) { c(*this); },
			            [this](const ncxxforth::tcell::String &c) { stack.push(c); },
			            [this](const ncxxforth::tcell::StringView &c) { stack.push(c); },
			           },
			           step);
			if (breaks > 0) {
				breaks--;
				break;
			}
		}
		executing   = exec_parent;
		exec_parent = old_parent;
	}
}
