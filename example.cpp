#include "clang.hpp"
#include "ncxxforth.config.hpp"
#include "ncxxforth.hpp"

#include <ios>
#include <iostream>
#include <ostream>
#include <variant>

using namespace ncxxforth;
using namespace std::literals;

namespace {
	[[clang::no_destroy]] static Word stack_one{{
	                                             [](Forth &f) {
		                                             if (f.stack.size() < 1) {
			                                             std::cerr << "Stack empty!" << std::endl;
			                                             f.breaks = 2;
		                                             }
	                                             },
	                                            },
	                                            false};

	[[clang::no_destroy]] static Word stack_drop{{
	                                              &stack_one,
	                                              [](Forth &f) { f.stack.pop(); },
	                                             },
	                                             false};

	[[clang::no_destroy]] static Word stack_num{{
	                                             &stack_one,
	                                             [](Forth &f) {
		                                             if (!std::holds_alternative<tcell::Number>(f.stack.top())) {
			                                             std::cerr << "Expected Number: " << std::to_string(f.stack.top())
			                                                       << std::endl;
			                                             f.breaks = 2;
		                                             }
	                                             },
	                                            },
	                                            false};

	[[clang::no_destroy]] static Word stack_min{{
	                                             &stack_one,
	                                             &stack_num,
	                                             [](Forth &f) {
		                                             auto i = static_cast<size_t>(std::get<tcell::Number>(f.stack.top()));
		                                             f.stack.pop();
		                                             if (f.stack.size() < i) {
			                                             std::cerr << "Expected Stack >= " << i << std::endl;
			                                             f.breaks = 2;
		                                             }
	                                             },
	                                            },
	                                            false};

	[[clang::no_destroy]] static Word pop_to_local{{
	                                                2,
	                                                &stack_min,
	                                                &stack_num,
	                                                [](Forth &f) {
		                                                auto i = static_cast<size_t>(std::get<tcell::Number>(f.stack.top()));
		                                                f.stack.pop();
		                                                f.exec_parent->content[i] = f.stack.top();
		                                                f.stack.pop();
	                                                },
	                                               },
	                                               false};

	[[clang::no_destroy]] static Word skip_n{{
	                                          &stack_one,
	                                          &stack_num,

	                                          [](Forth &f) {
		                                          f.breaks = 1;
		                                          f.jumps  = std::get<tcell::Number>(f.stack.top()) + 1;
		                                          f.stack.pop();
	                                          },
	                                         },
	                                         true};

	[[clang::no_destroy]] static Word add{{
	                                       2,
	                                       &skip_n,
	                                       0, // placeholder a
	                                       0, // placeholder b

	                                       2,
	                                       &stack_min,

	                                       &stack_num,
	                                       2, // a
	                                       &pop_to_local,

	                                       &stack_num,
	                                       3, // b
	                                       &pop_to_local,

	                                       [](Forth &f) {
		                                       f.stack.push(std::get<tcell::Number>(f.executing->content[2])
		                                                    + std::get<tcell::Number>(f.executing->content[3]));
	                                       },
	                                      },
	                                      false};

	[[clang::no_destroy]] static Word semi{{
	                                        [](Forth &f) { f.breaks = 2; },
	                                       },
	                                       true};

	[[clang::no_destroy]] static Word colon{{
	                                         [](Forth &f) {
		                                         tcell::String name = f.read_word();
		                                         Word *word         = new Word(f.compile_inplace());
		                                         f.dict.insert_or_assign(name, word);
	                                         },
	                                        },
	                                        false};

	[[clang::no_destroy]] static Word period{{
	                                          &stack_one,
	                                          [](Forth &f) {
		                                          std::cout << std::to_string(f.stack.top(), &f) << consts::svENDL;
		                                          // formatting borked
	                                          },
	                                          &stack_drop,
	                                         },
	                                         false};
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char *argv[]) {
	Forth f{{
	 {"+", &add},
	 {":", &colon},
	 {";", &semi},
	 {".", &period},
	}};

	Word w = f.compile("-0xa");
	f.dict.insert_or_assign("min10", &w);

	f.eval(": sub10 min10 + ;");
	f.eval("15 sub10 .");

	Cell cw = &add;
	std::cout << "&add: " << to_string(cw) << " " << to_string(cw, &f) << std::endl;

	return 0;
}
