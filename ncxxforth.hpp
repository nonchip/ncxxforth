#pragma once
//#include "clang.hpp"
#include "ncxxforth.config.hpp"

#include <deque>
#include <stack>
#include <unordered_map>
#include <variant>
#include <vector>

namespace ncxxforth {
	struct Word;

	// Cell is the underlying variant datatype we use for everything
	using Cell = std::variant<tcell::Number,      // integers
	                          Word *,             // we don't want to abuse tcell::NativePtr for XTs
	                          tcell::NativePtr,   // data pointers
	                          tcell::NativeWord,  // function pointers: void(Forth&)
	                          tcell::String,      // strings
	                          tcell::StringView>; // string view

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpadded"
	struct Word { // Words are just some flags and a bunch of Cells
			std::vector<Cell> content;
			bool immediate;
			// end padded to size.
	};
#pragma clang diagnostic pop

	// DictionaryEntries are name -> Word*
	using DictionaryEntry = std::pair<tcell::String, Word *>;
	// a Dictionary is a lot of those.
	using Dictionary      = std::unordered_map<DictionaryEntry::first_type, DictionaryEntry::second_type>;

	using Stack = std::stack<Cell>;

	struct Forth {
			Forth();
			Forth(Dictionary _dict, tcell::StringView _init = "");
			~Forth();

			void eval(tcell::StringView source);
			Word compile(tcell::StringView source);
			void eval_inplace();
			Word compile_inplace();

			void comp1(tcell::StringView word);
			void interp1(tcell::StringView word);
			void exec1(Word &word);
			tcell::String read_word(tcell::StringView del = consts::svDELIMS);

			tcell::StringView dictFind(Word *ptr) const;

			Stack stack;
			Dictionary dict;
			Word *compiling = nullptr;
			Word *executing   = nullptr;
			Word *exec_parent = nullptr;
			tcell::Number breaks, jumps;
			tcell::StringView src;
			tcell::NativePtr src_offs;
	};

	bool is_number(tcell::StringView word);
	/*constexpr*/ tcell::Number strvton(tcell::StringView str);

	template <class... Ts> struct visitor : Ts... { using Ts::operator()...; };
	template <class... Ts> visitor(Ts...) -> visitor<Ts...>;
}

namespace std {
	std::string to_string(const ncxxforth::Cell &, const ncxxforth::Forth * = nullptr);
}
