# ncxxforth

a modernized (= as much C++20 as my clang supports) Forth implementation loosely inspired by [Kristopher Johnson's "cxxforth"](https://github.com/kristopherjohnson/cxxforth) and [general concepts from the CollapseOS documentation](https://incoherency.co.uk/collapseos/impl.txt.html)
