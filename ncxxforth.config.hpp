#pragma once
//#include "clang.hpp"

#include <cstddef>
#include <string>
#include <string_view>
#include <variant>

namespace ncxxforth {
	struct Forth;
	namespace tcell { // cell types:
		// explicit, might want to edit:
		using Number    = long;        // e.g. literal
		using NativePtr = uintptr_t;   // native data pointer
		using String    = std::string; // mutable string
		// derived, usually no reason to edit:
		using NativeWord = void (*)(Forth &);                          // native function pointer
		using StringView = std::basic_string_view<String::value_type>; // immutable (read-only) string view
	}

	namespace consts { // some constants:
		// i guess modify those as a last ditch bandaid for dealing with weird terminals:
		constexpr tcell::StringView svDELIMS = " \t\r\n\v"; // word delimiters
		constexpr tcell::StringView svENDL   = "\n";        // end-of-line sequence

		// you usually shouldn't have to touch these:
		constexpr tcell::Number nFALSE = 0;       // Forth says false is 0b00000...
		constexpr tcell::Number nTRUE  = ~nFALSE; // and true is 0b11111...
	}
}
